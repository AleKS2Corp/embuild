# emBuild

[![pipeline status](https://gitlab.com/AleKS2Corp/embuild/badges/main/pipeline.svg)](https://gitlab.com/AleKS2Corp/embuild/-/commits/main)
[![Latest Release](https://gitlab.com/AleKS2Corp/embuild/-/badges/release.svg)](https://gitlab.com/AleKS2Corp/embuild/-/releases)

SEGGER Embedded Studio docker image for building projects in CI/CD.

## Built-in packages:
* libcxx_arm_v6m
* libcxx_arm_v7em

## Useful links
* [Embedded Studio](https://www.segger.com/downloads/embedded-studio/)