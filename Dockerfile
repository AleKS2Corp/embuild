FROM mcr.microsoft.com/powershell:ubuntu-22.04

ENV INSTALL_SCRIPT=install.sh

ADD ${INSTALL_SCRIPT} .

ADD stdint.h .

RUN ./${INSTALL_SCRIPT}

CMD ["/bin/bash"]