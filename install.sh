#!/bin/bash

# INSTALL GIT AND SES
apt-get update
apt-get install -y libx11-6 libfreetype6 libxrender1 libfontconfig1 libxext6 xvfb git wget libusb-1.0-0-dev

Xvfb :1 -screen 0 1024x768x16 &

mkdir -p /_tmp
cd /_tmp
wget --no-check-certificate -qO SES https://www.segger.com/downloads/embedded-studio/Setup_EmbeddedStudio_ARM_v730_linux_x64.tar.gz
tar xvf SES
printf 'yes\n' | DISPLAY=:1 $(find arm_segger_* -name "install_segger*") --copy-files-to /ses && \
cd /
rm -rf /_tmp

./ses/bin/pkg update
./ses/bin/pkg install -yes libcxx_arm libcxx_arm_v7em_le libcxx_arm_v7m_le libcxx_arm_v6m_le

apt-get -y --purge autoremove wget
apt-get clean

# SET LINK TO POWERSHELL
ln -s /usr/bin/pwsh /usr/bin/powershell

# SET LINK TO emBuild
ln -s /ses/bin/emBuild /usr/bin/emBuild

mv -f stdint.h ./ses/include

#SELF REMOVED
rm -- "$0"
