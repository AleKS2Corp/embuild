# Changelog

## 7.30 (2023-06-30)

#### New Features

* :sparkles: update segger to 7.30

Full set of changes: [`7.22...7.30`](https://gitlab.com/AleKS2Corp/embuild/compare/7.22...7.30)

## 7.22 (2023-06-30)

#### New Features

* :sparkles: update segger to 7.22

Full set of changes: [`7.20...7.22`](https://gitlab.com/AleKS2Corp/embuild/compare/7.20...7.22)

## 7.20 (2023-06-30)

#### New Features

* :sparkles: update segger to 7.20

Full set of changes: [`7.12a...7.20`](https://gitlab.com/AleKS2Corp/embuild/compare/7.12a...7.20)

## 7.12a (2023-03-20)

#### New Features

* :sparkles: update segger to 7.12a

Full set of changes: [`7.12...7.12a`](https://gitlab.com/AleKS2Corp/embuild/compare/7.12...7.12a)

## 7.12 (2023-03-14)

#### New Features

* :sparkles: update segger to 7.12

Full set of changes: [`7.10a...7.12`](https://gitlab.com/AleKS2Corp/embuild/compare/7.10a...7.12)

## 7.10a (2023-01-17)

#### New Features

* :sparkles: update segger to 7.10a

Full set of changes: [`7.10...7.10a`](https://gitlab.com/AleKS2Corp/embuild/compare/7.10...7.10a)

## 7.10 (2022-11-25)

#### New Features

* :sparkles: update segger to 7.10

Full set of changes: [`6.40...7.10`](https://gitlab.com/AleKS2Corp/embuild/compare/6.40...7.10)

## 6.40 (2022-11-25)

#### New Features

* :sparkles: update segger to 6.40
* :sparkles: update segger to 6.34a
* :sparkles: update segger to 6.34
* :sparkles: added libcxx_arm_v6m
* :sparkles: update segger to 6.32b
#### Others

* :construction_worker: tag pattern for changelog

Full set of changes: [`6.32...6.40`](https://gitlab.com/AleKS2Corp/embuild/compare/6.32...6.40)

## 6.32 (2022-06-22)

#### New Features

* :sparkles: update segger to 6.32

Full set of changes: [`6.30...6.32`](https://gitlab.com/AleKS2Corp/embuild/compare/6.30...6.32)

## 6.30 (2022-05-12)

#### New Features

* :sparkles: update segger to 6.30
* :tada: dockerfile and scripts
